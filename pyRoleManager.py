## Text menu in Python
from platform import system as system_name # Returns the system/OS name
from os import system as system_call       # Execute a shell command
import cfgData
import charMenu
import charData
import export
import exp
import charCreate
import charImport
import skill
import charChange
import level
import time

def print_menu():
    print 30 * "=" , "PyRoleManager" , 30 * "="
    print 30 * " " , "  Main Menu  " , 30 * " "
    print "1. New Characters"
    print "2. Import Character"
    print "3. Add skills for imported character"
    print "4. Export Character Sheet"
    print "5. Print list of all skills to screen"
    print "6. Set Miscellaneous Stat Bonus"
    print "7. Set Miscellaneous Skill Bonus"
    print "8. Set Background Options"
    print "9. Set Languages"
    print
    print 20 * "-", "Raise Level" , 20 * "-"
    print
    print "10. Add experience to character"
    print "11. Add/increase skills"
    print "12. Add Hit Points"
    print "13. Stat Gain Roll"
    print
    print 20 * "-", "Modify Character" , 20 * "-"
    print
    print "14. Assign/Change character's Armor/Helm/Shield Type"
    print "15. Change physical attributes/Background Options"
    print
    print ""
    print "X. Exit"
    print 45 * "-"

# Clear the clear_screen
cfgData.clear_screen()

###########################
###   Start Menu Loop   ###
###########################

loop=True

while loop:          ## While loop which will keep going until loop = False
    print_menu()    ## Displays menu
    choice = raw_input("Enter your choice [1-14]: ")
    print ""
    if choice=="1":
        cfgData.clear_screen()
        charCreate.create_char()
    elif choice=="2":
        cfgData.clear_screen()
        charImport.char_import()
    elif choice=="3":
        cfgData.clear_screen()
        p=charMenu.char_menu()
        menu_len=len(p)
        while True:
            s=int(raw_input("Select Character: "))
            if s >=1 and s<=menu_len:
                break
            else:
                print "Invalid Selection! Select a character from the list"

        # Open the file
        char_dict={}
        s-=1
        char = p[s]
        skill.import_skill(char)
    elif choice=="4":
        cfgData.clear_screen()
        export.export_to_excel()
        cfgData.clear_screen()
    elif choice=="5":
        cfgData.clear_screen()
        print "5"
    elif choice=="6":
        cfgData.clear_screen()
        charData.mbbonus()
        cfgData.clear_screen()
    elif choice=="8":
        cfgData.clear_screen()
        p=charMenu.char_menu()
        menu_len=len(p)
        while True:
            s=int(raw_input("Select Character: "))
            if s >=1 and s<=menu_len:
                break
            else:
                print "Invalid Selection! Select a character from the list"

        # Open the file
        char_dict={}
        s-=1
        char = p[s]
        charData.background_options(char)
        cfgData.clear_screen()
    elif choice=="10":
        cfgData.clear_screen()
        exp.exp_check()
        cfgData.clear_screen()
    elif choice=="11":
        p=charMenu.char_menu()
        menu_len=len(p)
        while True:
            s=int(raw_input("Select Character: "))
            if s >=1 and s<=menu_len:
                break
            else:
                print "Invalid Selection! Select a character from the list"

        # Open the file
        char_dict={}
        s-=1
        char = p[s]
        skill.skill_add(char)
        cfgData.clear_screen()
    elif choice=="12":
        cfgData.clear_screen()
        p=charMenu.char_menu()
        menu_len=len(p)
        while True:
            s=int(raw_input("Select Character: "))
            if s >=1 and s<=menu_len:
                break
            else:
                print "Invalid Selection! Select a character from the list"

        # Open the file
        char_dict={}
        s-=1
        char = p[s]
        charData.increase_hp(char)
        cfgData.clear_screen()
    elif choice=="13":
        cfgData.clear_screen()
        charData.stat_gain()
    elif choice=="14":
        cfgData.clear_screen()
        charData.assign_at()
    elif choice=="15":
        cfgData.clear_screen()
        charChange.chphy()
        cfgData.clear_screen()

    elif choice=="16":
        cfgData.clear_screen()
        p=charMenu.char_menu()
        menu_len=len(p)
        while True:
            s=int(raw_input("Select Character: "))
            if s >=1 and s<=menu_len:
                break
            else:
                print "Invalid Selection! Select a character from the list"

        # Open the file
        char_dict={}
        s-=1
        char = p[s]
        charData.spells(char)
    elif choice=="17":
        cfgData.clear_screen()
        level.exp_check()
        cfgData.clear_screen()

    elif choice=="x":
        print "Exiting Program"
        loop=False
    elif choice=="X":
        print "Exiting program"
        loop=False # This will make the while loop to end as not value of loop is set to False
    else:
        # Any integer inputs other than values 1-5 we print an error message
        raw_input("Wrong option selection. Enter any key to try again..")
        time.sleep(1)
        cfgData.clear_screen()
