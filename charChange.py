import cfgData
import json
import charMenu

def chphy():
    p=charMenu.char_menu()
    menu_len=len(p)
    while True:
        s=int(raw_input("Select Character: "))
        if s >=1 and s<=menu_len:
            break
        else:
            print "Invalid Selection! Select a character from the list"

    # Open the file
    char_dict={}
    s-=1
    char=p[s]
    with open(cfgData.char_dir+"/"+char+"/"+char+".json","r") as cf:
        char_dict = json.load(cf)

    loop=True
    while loop:
        print
        print "*", 23 * "=", "*"
        print "|", 23 * " ", "|"
        print "| 1.) Eye Color  ({:^6}) |".format(char_dict['eye'])
        print "| 2.) Hair Color ({:^6}) |".format(char_dict['hair'])
        print "| 3.) Appearance ({:^5})  |".format("app")
        print "| 4.) Weight     ({:^5})  |".format(char_dict['weight'])
        print "| 5.) Age        ({:^4})   |".format(char_dict['age'])
        print "| 6.) Background Options  |"
        print "|", 23 * " ", "|"
        print "| x.) Exit {:15}|".format(" ")
        print "*", 23 * "=", "*"
        print
        ch=raw_input("Select item to modify: ")
        if ch == "1":
            print "Current eye color is {:6}".format(char_dict['eye'])
            x=raw_input("Change to: ")
            char_dict['eye'] = x.capitalize()
            chsave(char,char_dict)
        elif ch == "2":
            print "Current hair color is {:6}".format(char_dict['hair'])
            x=raw_input("Change to: ")
            char_dict['hair']=x.capitalize()
            chsave(char,char_dict)
        elif ch == "3":
            print "Current appearance is {:<3}".format(char_dict['appear'])
            x=raw_input("Change to: ")
            char_dict['appear']=x
            chsave(char,char_dict)
        elif ch == "4":
            print "Current weight is {:<6}".format(char_dict['weight'])
            x=int(raw_input("Change to: "))
            char_dict['weight']=x
            chsave(char,char_dict)
        elif ch == "5":
            print "Current age is {:<6}".format(char_dict['age'])
            x=int(raw_input("Change to: "))
            char_dict['age']=x
            chsave(char,char_dict)
        elif ch == "6":
            print
            cnt=1
            #print char_dict['bgo']
            for x in char_dict['bgo']:
                print "{:1}.) {:<70}".format(cnt,x)
                cnt+=1
            print
            print "x.) exit"
            ch=raw_input("Select the number of the Background option to change: ")
            print ch,":ch"
            if ch.lower() == "x":
                break
            elif 0 < x > 9:
                j=raw_input("Enter Background Option: ")
                print j,":j"
                char_dict['bgo'][int(ch)-1] = j
                chsave(char,char_dict)
        elif ch.lower() == "x":
            print "Exiting..."
            loop=False
        else:
            print "Wrong selection. Please try again..."

def chsave(char,dict):
    with open(cfgData.char_dir+"/"+char+"/"+char+".json", 'w') as f:
        f.write(json.dumps(dict))
